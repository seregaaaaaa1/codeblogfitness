﻿using System;


namespace CodeBlogFitness.BL.Model
{
    [Serializable]
   public class Food
    {
        private readonly double calories;

        public int Id { get; set; }

        public string Name { get;  }

        /// <summary>
        /// Белки
        /// </summary>
        public double Proteins { get; }

        /// <summary>
        /// Жиры
        /// </summary>
        public double Fats { get; }

        /// <summary>
        /// Углеводы
        /// </summary>
        public double Carbohydrates { get; }

        /// <summary>
        /// Калории за 100 грамм продукта
        /// </summary>
        public double Calories { get; }
        
        public Food(string name) : this (name, 0,0,0,0 ) { }

        public Food(string name, double calories, double proteins, double fats, double carbohydrates)
        {
            //TODO: проверка
                      
            Name = name;
            Calories = calories / 100;
            Proteins = proteins / 100;
            Fats = fats / 100;
            Carbohydrates = carbohydrates / 100;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
